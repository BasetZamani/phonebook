package com.example.demo;

import com.example.demo.Interface.MyContactInterface;
import com.example.demo.model.MyContact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
public class DemoApplication implements CommandLineRunner {
    @Autowired
    MyContactInterface myContactInterface;

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        for (int i = 0; i < 100; i++)
            myContactInterface.save(new MyContact("baset"+i,"0918981401"+i,
                    "i.ixxxx"+i+"@gmail.com","irani","bata11"+i));

        List<MyContact> myContactList = (List<MyContact>) myContactInterface.findAll();
        for (MyContact myContact : myContactList)
            System.out.println(myContact);

    }
}
