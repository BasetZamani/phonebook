package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.util.Set;

@Entity(name = "MyContact")
@Setter
@Getter
@NoArgsConstructor
@ToString
public class MyContact {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @ToString.Exclude
    private long id;
    private String name;
    private String phoneNumber;
    @Email
    private String email;
    private String organization;
    @Column(unique = true)
    private String github;
    @OneToMany(mappedBy = "myContact")
    @ToString.Exclude
    private Set<Repository> repositories;

    public MyContact(String name, String phoneNumber,
                     @Email String email, String organization, String github) {
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.organization = organization;
        this.github = github;
    }
}
