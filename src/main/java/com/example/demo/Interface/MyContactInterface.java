package com.example.demo.Interface;

import com.example.demo.model.MyContact;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface MyContactInterface extends PagingAndSortingRepository<MyContact, Long>, JpaSpecificationExecutor<MyContact> {
}

