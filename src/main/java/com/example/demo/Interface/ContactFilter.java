package com.example.demo.Interface;

import com.example.demo.model.MyContact;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class ContactFilter implements Specification<MyContact> {

    private MyContact filter;

    public ContactFilter(MyContact filter) {
        this.filter = filter;
    }

    @Override
    public Predicate toPredicate(Root<MyContact> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        List<Predicate> temp = new ArrayList<>();


        //**************************************************************************************************************
        if (filter.getEmail() != null) {
            temp.add(criteriaBuilder.like(root.get("email"), "%" + filter.getEmail() + "%"));
        }
        //**************************************************************************************************************
        if (filter.getGithub() != null) {
            temp.add(criteriaBuilder.like(root.get("github"), "%" + filter.getGithub() + "%"));
        }
        //**************************************************************************************************************
        if (filter.getName() != null) {
            temp.add(criteriaBuilder.like(root.get("name"), "%" + filter.getName() + "%"));
        }
        //**************************************************************************************************************
        if (filter.getOrganization() != null) {
            temp.add(criteriaBuilder.like(root.get("organization"), "%" + filter.getOrganization() + "%"));
        }
        //**************************************************************************************************************
        if (filter.getPhoneNumber() != null) {
            temp.add(criteriaBuilder.like(root.get("phoneNumber"), "%" + filter.getPhoneNumber() + "%"));
        }
        //**************************************************************************************************************


        return criteriaBuilder.and(temp.toArray(new Predicate[0]));

    }
}
